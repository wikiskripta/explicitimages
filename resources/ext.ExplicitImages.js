/**
 * Blur images with "(explicit" in filename
 */

( function ( mw, $ ) {

	// remove fallback if browser is recent
	var browserIsRecent = false;
	if(jQBrowser.name == "chrome" && jQBrowser.versionNumber > 17 ) browserIsRecent = true;
	else if(jQBrowser.name == "msedge" && jQBrowser.versionNumber > 12 ) browserIsRecent = true;
	else if(jQBrowser.name == "mozilla" && jQBrowser.versionNumber > 34 ) browserIsRecent = true;
	else if(jQBrowser.name == "safari" && jQBrowser.versionNumber > 5 ) browserIsRecent = true;
	else if(jQBrowser.name == "opera" && jQBrowser.versionNumber > 12 ) browserIsRecent = true;
	else if(jQBrowser.name.indexOf("android") !== -1 && jQBrowser.versionNumber > 4 ) browserIsRecent = false;
	else if(jQBrowser.name.indexOf("blackberry") !== -1 && jQBrowser.versionNumber > 7 ) browserIsRecent = false;
	if(browserIsRecent) deactivateFallback();

	$(".overlay-image").each(function( index ) {				
		// disable href - images/file page 
		if(mw.config.get("wgNamespaceNumber") == 6) {
			$(this).parent().on('click', function(e) {
				e.preventDefault();
				unlockExtensiveImages();
			});
		}
		else {
			$(this).find(">:first-child").on('click', function(e) {
				e.preventDefault();
				unlockExtensiveImages();
			});
		}
	});

	// disable text links to image
	$("td.filehistory-selected a").on('click', function(e) {
		e.preventDefault();
		unlockExtensiveImages();
	});
	$(".fullMedia a").on('click', function(e) {
		e.preventDefault();
		unlockExtensiveImages();
	});

}( mediaWiki, jQuery ) );


/**
 * Unlock all extensive images at page
 */
function unlockExtensiveImages() {
	$(".overlay-image").parent().css("pointer-events", "none");
	$(".overlay-image").parent().css("cursor", "default");
	$("td.filehistory-selected a").css("pointer-events", "none");
	$("td.filehistory-selected a").css("cursor", "default");
	$(".fullMedia a").css("pointer-events", "none");
	$(".fullMedia a").css("cursor", "default");
	noty({
		text: mw.message("explicitimages-warning").text(),
		theme:'relax', 
		layout:'topCenter',
		closeWith: [],
		buttons: [
			{addClass: 'mw-ui-button mw-ui-destructive notyButtonOk', text: mw.message("explicitimages-reveal").text(), onClick: function($noty) {
					$noty.close();
					$(".overlay-image").parent().css("pointer-events", "auto");
					$(".overlay-image").parent().css("cursor", "pointer");
					$(".overlay-image").parent().off("click");
					$("td.filehistory-selected a").css("pointer-events", "auto");
					$("td.filehistory-selected a").css("cursor", "pointer");
					$("td.filehistory-selected a").off("click");
					$(".fullMedia a").css("pointer-events", "auto");
					$(".fullMedia a").css("cursor", "pointer");
					$(".fullMedia a").off("click");
					$(".overlay-image img").removeClass("noviewer").css("filter", "");
					$(".overlay-image").each(function( index ) {				
						if(mw.config.get("wgNamespaceNumber") == 6) $(this).parent().off('click');
						else $(this).find(">:first-child").off('click');
					});
					deactivateFallback();
					$(".overlay-text").remove();
				}
			},
			{addClass: 'mw-ui-button mw-ui-constructive notyButtonCancel', text: mw.message("explicitimages-cancel").text(), onClick: function($noty) {
					$noty.close();
					$(".overlay-image").parent().css("pointer-events", "auto");
					$(".overlay-image").parent().css("cursor", "pointer");
					$("td.filehistory-selected a").css("pointer-events", "auto");
					$("td.filehistory-selected a").css("cursor", "pointer");
					$(".fullMedia a").css("pointer-events", "auto");
					$(".fullMedia a").css("cursor", "pointer");
				}
			}
		],
	});
}


/**
 * Fallback deactivation
 */
function deactivateFallback() {
	$(".overlay-image img").css("position","");
   	$(".overlay-image img").css("width","");
	$(".overlay-image img").css("height","");
	$(".overlay-image img").css("background-color","");
   	$(".overlay-image img").css("opacity","");
   	$(".overlay-image img").css("-moz-opacity","");
	$(".overlay-image img").css("-webkit-opacity","");
	$(".overlay-image img").css("z-index","");
	$(".overlay-image img").css("border","");
}
