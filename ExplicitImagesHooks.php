<?php

/**
 * All hooked functions used by ExplicitImages
 * @ingroup Extensions
 * @author Josef Martiňák
 * @license MIT
 * @file
 */

class ExplicitImagesHooks {

	/**
	 * Search and blur all explicit images
	 * @param object $out: instance of OutputPage
	 * @param object $skin: instance of Skin, unused
	 */
	public static function blurImages( &$out, &$skin ) {

		if ( $out->isArticle() && preg_match( "/\(explicit/", $out->mBodytext ) ) {

			// Disable MediaViewer for explicit images and blur image
			$replaceText = '<div class="overlay-image"><img $1src="$2" $3 class="noviewer" style="filter:blur(10px);position:relative;background-color: black;opacity: 0.03;-moz-opacity: 3%;-webkit-opacity: 3%;z-index: 2;border:1px solid black;">';
			$replaceText .= '<div class="overlay-text"><div class="explicit-warning" title="' . $out->msg('explicitimages-warning')->text() . ' ' . $out->msg('explicitimages-reveal')->text() . '">' . $out->msg('explicitimages-warning-link')->text() . '</div></div></div>';
			$out->mBodytext = preg_replace('/<img (.*?)src="(.*?%28explicit[^\"]*)" (.*?)\/*>/', $replaceText, $out->mBodytext);
			$out->addModules( 'ext.ExplicitImages' );
		}
		return true;
	}
}
