# ExplicitImages

Mediawiki extension.

## Description

* Extension blurs explicit images and offers a button to show the original image.
* Explicit image must contain "(explicit)" or "(explicitní)" in its name.
* Fallback for old browsers - opacity instead of blur filter.

## Installation

* Make sure you have MediaWiki 1.32+ installed.
* Download and place the extension to your _/extensions/_ folder.
* Add the following code to your LocalSettings.php: `wfLoadExtension( 'ExplicitImages' )`;

## Internationalization

This extension is available in English and Czech language. For other languages, just edit files in /i18n/ folder.

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2019 First Faculty of Medicine, Charles University

## Third party plugins

* [Noty - A jQuery Notification Plugin](https://github.com/needim/noty)
* [JQuery browser plugin](https://github.com/gabceb/jquery-browser-plugin)
